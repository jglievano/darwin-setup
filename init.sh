#!/usr/bin/env bash

stow_to () {
  declare dst="$1" pkg="$2"
  echo "Intalling ${pkg:?} to ${dst:?}"
  stow --verbose=1 -t "${dst:?}" "$pkg"
}

CLOUD_DRIVE_PATH=${HOME}/Dropbox

GO_VERSION="1.12.4"
RUBY_VERSION="2.6.3"
NVM_VERSION="0.34.0"
NODE_CUR_VERSION="12.0.0"
NODE_LTS_VERSION="10.15.3"

chmod 0700 ~/.ssh/
chmod 0644 ~/.ssh/id_rsa.pub
chmod 0600 ~/.ssh/id_rsa

/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install bash cmake curl fd fzf git go hugo neovim openssl python \
  reattach-to-user-namespace shellcheck sqlite stow the_silver_searcher \
  tmux tree vim
brew cask install emacs

mkdir -p ~/src

[[ -d "${HOME}/src/dotfiles" ]] || \
  git clone git@gitlab.com:jglievano/dotfiles.git ${HOME}/src/dotfiles

rm ~/.bashrc

cd "${HOME}/src/dotfiles" || exit
stow_to "${HOME}" bin
stow_to "${HOME}" bash
stow_to "${HOME}" emacs
stow_to "${HOME}" git
stow_to "${HOME}" vim
stow_to "${HOME}" tmux
cd || exit

ln -s .bashrc .profile
ln -s .bashrc .bash_profile

if ! [ -x "$(command -v rbenv >/dev/null 2>&1)" ]; then
  [[ -d "${HOME}/.rbenv" ]] || git clone https://github.com/rbenv/rbenv.git ~/.rbenv
  [[ -d "${HOME}/.rbenv/plugins/ruby-build" ]] || \
    git clone https://github.com/rbenv/ruby-build.git \
      ~/.rbenv/plugins/ruby-build
  # shellcheck source=/dev/null
  source ~/.bashrc
fi

rbenv install "${RUBY_VERSION}"
rbenv global "${RUBY_VERSION}"

mkdir -p ~/.tmux/plugins
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

command -v tmuxinator >/dev/null 2>&1 || (
  gem install tmuxinator >/dev/null 2>&1
)
rbenv rehash

command -v rustc >/dev/null 2>&1 || (
  curl https://sh.rustup.rs -sSf | sh >/dev/null 2>&1
)

command -v go >/dev/null 2>&1 || (
  wget https://dl.google.com/go/go${GO_VERSION}.${GO_SYSTEM}-amd64.tar.gz >/dev/null 2>&1
  tar -xvf go${GO_VERSION}.${GO_SYSTEM}-amd64.tar.gz >/dev/null 2>&1
  sudo mv go /usr/local
)

[[ -d "${HOME}/.nvm" ]] || mkdir ~/.nvm
command -v nvm >/dev/null 2>&1 || (
  curl -o- \
    https://raw.githubusercontent.com/creationix/nvm/v${NVM_VERSION}/install.sh \
      | bash >/dev/null 2>&1
)

# shellcheck source=/dev/null
source ~/.bashrc

nvm install $NODE_LTS_VERSION
nvm install $NODE_CUR_VERSION
nvm use $NODE_CUR_VERSION

npm i -g csslint dockerfile_lint eslint fixjson htmlhint jsonlint \
  prettier prettier-eslint-cli prettier-standard sass-lint scss-lint \
  stylelint typescript typescript-language-server
pip3 install --upgrade autopep8 python-language-server vim-vint
go get github.com/mrtazz/checkmake
go get github.com/ckaznocha/protoc-gen-lint
go get github.com/jackc/sqlfmt/...
go get -u github.com/saibing/bingo
rustup update
rustup component add rustfmt --toolchain nightly
rustup component add rls rust-analysis rust-src

if ! [ -x "$(command -v exa >/dev/null 2>&1)" ]; then
  cargo install exa >/dev/null 2>&1
fi

[[ -e "${HOME}/.vimrc" ]] || ln -s ~/.vim/vimrc.vim ~/.vimrc
vim +PlugInstall +qall

[[ -e "${HOME}/.local.git" ]] || (
  read -r -p "Git user name: " git_name
  read -r -p "Git email: " git_email
  printf "[user]\n\tname = %s\n\temail = %s\n" "$git_name" "$git_email" \
    > ~/.local.git
)
